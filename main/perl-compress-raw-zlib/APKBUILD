# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=perl-compress-raw-zlib
_pkgname=Compress-Raw-Zlib
pkgver=2.205
pkgrel=0
pkgdesc="Perl low-level interface to zlib compression library"
url="https://metacpan.org/release/Compress-Raw-Zlib"
arch="all"
license="GPL-1.0-or-later OR Artistic-1.0-Perl"
depends="perl"
makedepends="perl-dev zlib-dev"
source="https://cpan.metacpan.org/authors/id/P/PM/PMQS/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	PERL_MM_USE_DEFAULT=1 BUILD_ZLIB=0 perl Makefile.PL INSTALLDIRS=vendor
	make
}

check() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete

	# man pages are already provided by perl-doc
	rm -rf "$pkgdir"/usr/share/man
}

sha512sums="
567abb51dc9939f0e0bc206202e63cd8805f924ef23ca48eae4992fdf97d58167aa7438732abd4cd27fa83b3849c7b263b55f3ec10009dddab8c9ca0d1a0ec18  Compress-Raw-Zlib-2.205.tar.gz
"
