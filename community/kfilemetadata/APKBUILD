# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kfilemetadata
pkgver=5.108.0
pkgrel=1
pkgdesc="A library for extracting file metadata"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends_dev="
	attr-dev
	exiv2-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	qt5-qtbase-dev
	taglib-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	ffmpeg-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kfilemetadata-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kfilemetadata.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# Broken tests
	local skipped_tests="("
	local tests="
		usermetadatawritertest
		extractorcoveragetest
		propertyinfotest_localized
		extractorcollectiontest
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f1a8d20ae91809f736d0355a3fafb9be0e77482b8c7fd6a5ff66f0f6937f4ad61c26841d9bcee2574fdb55f6dcfc1670ec26fef70f3f2a0613ba6a30a7826489  kfilemetadata-5.108.0.tar.xz
"
