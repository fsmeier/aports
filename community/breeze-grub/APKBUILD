# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=breeze-grub
pkgver=5.27.6
pkgrel=0
pkgdesc="Breeze theme for GRUB"
arch="noarch !s390x !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/plasma-desktop/"
license="GPL-3.0-or-later"
depends="grub"
makedepends="
	extra-cmake-modules
	font-unifont
	grub-mkfont
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-grub-$pkgver.tar.xz"
options="!check" # No test suite available

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/breeze-grub.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	./mkfont.sh
}

package() {
	install -d "$pkgdir"/usr/share/grub/themes
	cp -r breeze "$pkgdir"/usr/share/grub/themes
}
sha512sums="
1f75d7b2cb36c88ac26f2f609592d5b6129523a898499dff5193f3ed1e7d182b49d2c39a8e04f6a90553a4610f7b7dd3259159576cb6de3c86e8fa12d07472b9  breeze-grub-5.27.6.tar.xz
"
